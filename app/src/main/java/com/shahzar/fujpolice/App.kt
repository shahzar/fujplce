package com.shahzar.fujpolice

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.util.Log
import com.shahzar.fujpolice.utils.Utils

open class App : Application() {

    var lang : String = "en"

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(Utils.setLocale(base!!, lang))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Utils.setLocale(this, lang)
    }

}