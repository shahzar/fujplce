package com.shahzar.fujpolice

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.shahzar.fujpolice.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_landing.*
import java.util.*

class LandingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        btn_eng.setOnClickListener { launchMain("en") }
        btn_ar.setOnClickListener { launchMain("ar") }
    }

    private fun launchMain(lang: String) {
        // Set language
        (application as App).lang = lang
        val intent = Intent(this, NavActivity::class.java)
        startActivity(intent)
    }

}
