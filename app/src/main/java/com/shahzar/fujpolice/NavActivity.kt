package com.shahzar.fujpolice

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.widget.Toolbar
import com.shahzar.fujpolice.ui.FeedbackActivity
import com.shahzar.fujpolice.ui.base.BaseActivity
import com.shahzar.fujpolice.utils.PermissionUtil
import com.shahzar.fujpolice.utils.Utils
import kotlinx.android.synthetic.main.app_bar_nav.*

class NavActivity : BaseActivity() {

    private val reqCode: Int = 1337
    private val permissionReqCode: Int = 137
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                //todo add all menu items
                R.id.nav_home
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun init() {
        btn_feedback.setOnClickListener { startActivityForResult(Intent(this, FeedbackActivity::class.java), reqCode) }

        sos.setOnClickListener {
            val permission = Manifest.permission.CALL_PHONE
            if (!PermissionUtil.hasPermission(this, permission)) {
                PermissionUtil.requestPermission(this, arrayOf(permission), permissionReqCode)
                return@setOnClickListener
            }

           initEmergencyCall()
        }
    }

    private fun initEmergencyCall() {
        // showMessage("Done")
        Utils().callEmergency(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != reqCode) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        showMessage(getString(R.string.feedback_thanks))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (requestCode != permissionReqCode) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initEmergencyCall()
        }

    }
}
