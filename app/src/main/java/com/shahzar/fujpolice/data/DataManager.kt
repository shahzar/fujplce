package com.shahzar.fujpolice.data

import android.content.Context
import com.shahzar.fujpolice.R
import com.shahzar.fujpolice.data.model.EService

class DataManager() {

    fun getEServices(context: Context) : List<EService> {

        var list: MutableList<EService> = mutableListOf()

        list.add(EService(context.getString(R.string.service_employee), R.drawable.employee_services))
        list.add(EService(context.getString(R.string.service_citizen), R.drawable.customer_services))
        list.add(EService(context.getString(R.string.service_vendor), R.drawable.partner_services))
        list.add(EService(context.getString(R.string.service_pofd), R.drawable.differently_abled_person))
        list.add(EService(context.getString(R.string.service_dls), R.drawable.employee_services))
        list.add(EService(context.getString(R.string.service_scs), R.drawable.customer_services))
        list.add(EService(context.getString(R.string.service_rcc), R.drawable.partner_services))
        list.add(EService(context.getString(R.string.service_gaps), R.drawable.differently_abled_person))
        list.add(EService(context.getString(R.string.service_sls), R.drawable.employee_services))
        list.add(EService(context.getString(R.string.service_hfls), R.drawable.customer_services))
        list.add(EService(context.getString(R.string.service_esas), R.drawable.partner_services))
        list.add(EService(context.getString(R.string.service_cwc), R.drawable.differently_abled_person))


        return list
    }

}