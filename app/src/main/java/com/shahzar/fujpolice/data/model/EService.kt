package com.shahzar.fujpolice.data.model

import androidx.annotation.DrawableRes

data class EService(
    val name:String,
    @DrawableRes val imageRef:Int
    )