package com.shahzar.fujpolice.ui

import android.os.Bundle
import android.os.PersistableBundle
import com.shahzar.fujpolice.R
import com.shahzar.fujpolice.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_feedback.*

class FeedbackActivity : BaseActivity() {

    companion object {
        val RESULT_NEGATIVE: Int = 0
        val RESULT_NEUTRAL: Int = 1
        val RESULT_POSITIVE: Int = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        initView()
    }

    private fun initView() {
        close.setOnClickListener { finish() }
        response_negative.setOnClickListener { setResult(RESULT_NEGATIVE);finish()}
        response_neutral.setOnClickListener { setResult(RESULT_NEUTRAL);finish() }
        response_positive.setOnClickListener { setResult(RESULT_POSITIVE);finish() }
    }

}