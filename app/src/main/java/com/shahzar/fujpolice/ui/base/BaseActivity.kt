package com.shahzar.fujpolice.ui.base

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.shahzar.fujpolice.App
import com.shahzar.fujpolice.R
import com.shahzar.fujpolice.utils.Utils

open class BaseActivity: AppCompatActivity() {

    fun showMessage(msg: String) {
        window?.decorView?.rootView?.let {
            val snackbar = Snackbar.make(it, msg, Snackbar.LENGTH_LONG)
            snackbar.view.setBackgroundColor(resources.getColor(R.color.snack_bar_positive))
            snackbar.show()
        }
    }

    override fun attachBaseContext(newBase: Context?) {

        newBase?.applicationContext?.let {
            super.attachBaseContext(Utils.setLocale(newBase, (it as App).lang))
            return
        }

        super.attachBaseContext(newBase)

    }



}