package com.shahzar.fujpolice.ui.home

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.shahzar.fujpolice.R
import com.shahzar.fujpolice.data.DataManager
import com.shahzar.fujpolice.data.model.EService
import com.shahzar.fujpolice.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment() {

    private lateinit var root: View
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_home, container, false)
        // val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(this, Observer {
            // textView.text = it
        })

        viewInit()

        return root
    }

    fun viewInit() {
        root.service_list.layoutManager = LinearLayoutManager(activity)
        val serviceListAdapter = ServiceListAdapter()
        root.service_list.adapter = serviceListAdapter

        serviceListAdapter.add(DataManager().getEServices(context!!))

        root.search.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val filteredList = filter(DataManager().getEServices(context!!), p0.toString())
                serviceListAdapter.replaceAll(filteredList)
                root.service_list.scrollToPosition(0)
            }
        })
    }

    fun filter(list: List<EService>, query: String) : List<EService>{
        val lowerCaseQuery = query.toLowerCase()
        val filteredList: MutableList<EService> = mutableListOf()

        for (item in list) {
            val lowerCaseNmae = item.name.toLowerCase()
            if (lowerCaseNmae.contains(lowerCaseQuery)) {
                filteredList.add(item)
            }
        }

        return filteredList
    }
}