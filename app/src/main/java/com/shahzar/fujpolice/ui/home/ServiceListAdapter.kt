package com.shahzar.fujpolice.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import com.shahzar.fujpolice.R
import com.shahzar.fujpolice.data.model.EService
import kotlinx.android.synthetic.main.item_service_list.view.*



class ServiceListAdapter() : RecyclerView.Adapter<ViewHolder>() {

    private val mComparator = Comparator<EService> { a, b -> a.name.compareTo(b.name) }

    val sortedList: SortedList<EService> = SortedList(EService::class.java, object: SortedList.Callback<EService>(){
        override fun areItemsTheSame(item1: EService?, item2: EService?): Boolean {
            return item1?.name == item2?.name
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun compare(o1: EService?, o2: EService?): Int {
            return mComparator.compare(o1, o2)
        }

        override fun areContentsTheSame(oldItem: EService?, newItem: EService?): Boolean {
            return oldItem?.equals(newItem)!!
        }
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.iconView.setImageResource(sortedList.get(position).imageRef)
        holder.text.text = sortedList.get(position).name
    }

    override fun getItemCount(): Int {
        return sortedList.size()
    }

    fun add(item: EService) {
        sortedList.add(item)
    }

    fun remove(item: EService) {
        sortedList.remove(item)
    }

    fun add(items: List<EService>) {
        this.sortedList.addAll(items)
    }

    fun remove(items: List<EService>) {
        sortedList.beginBatchedUpdates()
        for (item in items) {
            sortedList.remove(item)
        }
        sortedList.endBatchedUpdates()
    }

    fun replaceAll(items: List<EService>) {
        sortedList.beginBatchedUpdates()
        for (i in sortedList.size() - 1 downTo 0) {
            val model = sortedList.get(i)
            if (!items.contains(model)) {
                sortedList.remove(model)
            }
        }
        sortedList.addAll(items)
        sortedList.endBatchedUpdates()
    }
}

class ViewHolder (view: View): RecyclerView.ViewHolder(view) {
    val iconView = view.service_icon
    val text = view.service_text
}