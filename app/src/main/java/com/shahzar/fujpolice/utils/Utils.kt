package com.shahzar.fujpolice.utils

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.util.Log
import java.util.*

class Utils {

    companion object {

        fun setLocale(context: Context, language: String): Context {

            var contextFun = context

            Log.d("LANG--", "Set to $language")
            val locale = Locale(language)
            Locale.setDefault(locale)

            val resources = context.resources
            val config = resources.configuration
            config.setLocale(locale)
            return context.createConfigurationContext(config)
        }

    }

    fun callEmergency(context: Context) {
        val num = "999"
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$num")
        context.startActivity(intent)
    }

}